#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
/// ajout de template
use rocket_contrib::templates::Template;
use std::collections::HashMap;

#[get("/")]
 /// permet une vue HTML
fn index() -> Template {
    let mut context:HashMap::new();
    Template::render("index",&context);
}

fn main() {
    rocket::ignite()
    .mount("/", routes![index])
    . attach ( Template :: fairing ());
    .launch();
}